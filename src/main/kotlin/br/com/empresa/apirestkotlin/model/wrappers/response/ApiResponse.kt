package br.com.empresa.apirestkotlin.model.wrappers.response

data class ApiResponse(
       val statusCode: Int = 200,
       val mensagem: String = "Operacao relizada com sucesso",
       val mensagens: ArrayList<String> = arrayListOf(),
       val data: Any? = null
)