package br.com.empresa.apirestkotlin.controller

import br.com.empresa.apirestkotlin.model.wrappers.request.CalculadoraRequest
import br.com.empresa.apirestkotlin.model.wrappers.response.ApiResponse
import br.com.empresa.apirestkotlin.service.CalculadoraService
import br.com.empresa.apirestkotlin.util.Utils
import br.com.empresa.apirestkotlin.validator.CalculoValidator
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("calculadora")
class ModuloController(
        private val calculadoraService: CalculadoraService,
        private val calculoValidator: CalculoValidator
        ) {

    @PostMapping
    fun calculadora(
            @RequestBody calculadoraRequest: CalculadoraRequest,
            bindingResult: BindingResult
    ): ApiResponse {

        calculoValidator.validate(calculadoraRequest, bindingResult)

        return if (!bindingResult.hasErrors()) {
            val resultado = calculadoraService.realizarCalculo(calculadoraRequest)
            ApiResponse(data = resultado)
        } else {
            ApiResponse(statusCode = 500, mensagem = "", mensagens = Utils.obterMensagens(bindingResult))
        }
    }

}